#!/bin/bash

OLMVERSION=3.1.2
SFVERSION=3.0.2.8

sudo chown nemo.nemo ~nemo/build

# get the develop environment

sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y python3-devel
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y gcc
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y gcc-c++
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y make
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y glib2-devel
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y libffi-devel
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R zypper install -y libjpeg-turbo-devel

cd ~nemo
mkdir -p build/lib

sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R python3 -m venv ~nemo/build/

# Download all needed stuff

#curl -O https://git.matrix.org/git/olm/snapshot/olm-$OLMVERSION.tar.gz
#tar xzf olm-$OLMVERSION.tar.gz
git clone https://gitlab.matrix.org/matrix-org/olm.git -b $OLMVERSION
# git clone https://github.com/poljar/python-olm
git clone https://github.com/Zil0/matrix-python-sdk.git

# Create libolm, install in system and copy libolm.so.3 to lib

pushd olm
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R make install
cp build/libolm.so.$OLMVERSION ~nemo/build/lib/libolm.so.3
popd

# build up and install poljar's python-olm

#pushd python-olm
#sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/python3 setup.py build
#sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/python3 setup.py install
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install python-olm
#cp -a .eggs/*.egg ~nemo/build/lib/python3.7/site-packages
#popd

# build up and install matrix-python-sdk e2e_beta_2

pushd matrix-python-sdk
git fetch origin e2e_beta_2
git checkout e2e_beta_2
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/python3 setup.py build
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/python3 setup.py install
popd

# Other packages

sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install pillow
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install emoji
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install markdown2
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install chardet
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install certifi
sb2 -t SailfishOS-$SFVERSION-armv7hl -m sdk-install -R ~nemo/build/bin/pip install idna
