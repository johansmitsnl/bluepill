import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    property string room_id
    property string room_name
    property bool pvae: true
    property bool renderflag: true
    property string mlname
    property string mavatar_url
    property string muser_id

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView {
        id: eventlist
        anchors.fill: parent
        width: parent.width
        height: contentHeight
        property string lastuser: ""
        property string lname: ""

        function set_lastuser(user) {
            eventlist.lastuser = user
        }

        header: PageHeader {
            id: title
            title: room_name
        }
        footer: TextControl {
            id: textcontrol
            visible: !renderflag
            user_id: muser_id
            avatar_url: mavatar_url
            lname: mlname
            room_id: { console.log("var room_id", page.room_id)
                return page.room_id
            }
        }


        PushUpMenu {
            MenuItem {
                text: qsTr('Preferences: ') + room_name
            }

            MenuItem {
                text: qsTr("Preferences")
                onClicked: pageStack.push(Qt.resolvedUrl("Preferences.qml"))
            }
        }

        Component.onCompleted: {
            renderflag = true
            console.log("entering room: getRoomEvents", room_id)
            clienthandler.getRoomEvents(room_id)
            clienthandler.getUserInfo()
            clienthandler.seenMessage(room_id)
        }

        Connections {
            target: clienthandler
            onRoomEvents: {
                console.log("in onRoomEvents")
                renderflag = false
                eventsModel.clear()
                for (var i=0; i < events.length; i++) {
                    if (eventlist.lastuser !== events[i].name || events[i].image !== "") {
                        eventlist.lname = events[i].name
                        eventlist.lastuser = eventlist.lname
                    } else {
                        eventlist.lname = ""
                    }

                    events[i].lname = eventlist.lname
                    eventsModel.append(events[i])
                }
                if(pvae) {
                    eventlist.positionViewAtEnd()
                }
                clienthandler.getRoomMembers(room_id)
            }
            onRoomMembers: {
                console.log("room members")
            }

            onMRoomEvent: {
                console.log("m.room.event", roomId, page.room_id)
                if (roomId !== page.room_id) {
                    return
                }

                if (eventlist.lastuser !== event.name) {
                    event.lname = event.name
                    eventlist.lname = event.name
                    eventlist.lastuser = eventlist.lname
                } else {
                    event.lname = ""
                }

                eventsModel.append(event)
                eventlist.positionViewAtEnd()
                clienthandler.seenMessage(room_id)
            }
            onUserInfo: {
                console.log("onUserInfo")
                mlname = userdata.lname
                mavatar_url = userdata.avatar_url
                muser_id = userdata.user_id
            }
        }

        ViewPlaceholder {
            id: placeholder
            enabled: renderflag
            text: qsTr("Rendering")
            hintText: qsTr("Collecting Posts")
        }
        section.property: 'asect'
        section.delegate: SectionHeader {
            text: section
            horizontalAlignment: Text.AlignRight
            font.bold: true
        }

        model: ListModel {
            id: eventsModel
            property string lastuser: ""
        }
        delegate: EventsListItem { }
    }
}
