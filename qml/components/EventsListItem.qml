import QtQuick 2.5
import Sailfish.Silica 1.0

ListItem {
    width: parent.width
    anchors {
        left: parent.left
        right: parent.right
    }
    contentHeight: thegrid.height

    menu: ContextMenu {
        MenuItem {
            text: qsTr("copy")
            visible: event.type === "m.room.message"
            onClicked: Clipboard.text = event.content.body
        }

        MenuItem {
            text: qsTr("event source")
            onClicked: {
                console.log("on event source clicked: " + JSON.stringify(event))
                pageStack.push(Qt.resolvedUrl("../pages/SourcePage.qml"), {eventsource: JSON.stringify(event, null, 4)})
            }
        }

        MenuItem {
            text: qsTr("cite")
            visible: event.type === "m.room.message"
            onClicked: {
                entertext = "> " + event.content.body.replace(/\r/gm, "\n> ") + "\n\n"
            }
        }

        MenuItem {
            text: qsTr("delete")
            visible: event.type === "m.room.message"
            onClicked: {
                pageStack.push(Qt.resolvedUrl("../pages/RedactMessage.qml"), { rid: room_id, evid: event.event_id,
                                          bd: event.content.body })
                eventlist.positionViewAtEnd()
            }
        }
    }

    Item {
        id: thegrid
        width: parent.width
        // columns: 2
        // spacing: 2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: Theme.paddingMedium
        height: eventItem.height > iconColumn.height ? eventItem.height : iconColumn.height

        property bool animated: false

        Connections {
            target: clienthandler
            onMRedactEvent: {
                if (event.event_id === redactEvent.redacts) {
                    event = redactEvent
                }
            }
        }

        Column {
            id: iconColumn
            anchors.margins: Theme.paddingMedium
            width: Theme.iconSizeMedium
            // anchors.left: parent.left
            UserIcon {
                anchors.horizontalCenter: parent.horizontalCenter
                width: event.type === "m.room.member" ? Theme.iconSizeSmall : Theme.iconSizeMedium
                height: event.type === "m.room.member" ? Theme.iconSizeSmall : Theme.iconSizeMedium
                uid: user_id
                avatarurl: avatar_url
                idirect: true
                roomname: lname
                visible: lname != ""
            }
            Rectangle {
                width: Theme.iconSizeMedium
                height: Theme.iconSizeMedium
                opacity: 0.0
                visible: lname == ""
            }
        }

        Column {
            id: eventItem
            anchors.left: iconColumn.right
            anchors.margins: Theme.paddingMedium
            width: page.width - iconColumn.width - iconColumn.spacing - 4 * Theme.paddingMedium
            height: usertime.height + usertime.spacing + msgLabel.height + msgLabel.spacing
            // anchors {
                // left: iconColumn.left
                // right: parent.right
            // }
            Row {
                id: usertime
                width: parent.width
                Label {
                    id: userLabel
                    horizontalAlignment: Text.AlignLeft
                    text: {
                            if (event.type === "m.room.member") {
                                try {
                                    return event.unsigned.prev_content.displayname
                                }
                                catch(e) {
                                    return event.content.displayname
                                }
                            }
                            return event.type === 'm.sticker' ? "" : (lname + (image !== "" ? qsTr(' has sent an image') : ''))
                    }
                    color: u_color(user_id)
                    font.bold: true
                    font.pixelSize: Theme.fontSizeExtraSmall * fontSizeConf.value
                }
                Label {
                    id: dateHour
                    width: parent.width - userLabel.width
                    horizontalAlignment: Text.AlignRight
                    text: {
                        var date = new Date(null)
                        date.setSeconds(edate - date.getTimezoneOffset() * 60)
                        return date.toLocaleTimeString().substring(0,5)
                    }

                    color: Theme.highlightColor
                    font.pixelSize: Theme.fontSizeTiny
                }

            }
    //        Image {
    //            id: imgLabel
    //            // asynchronous: true
    //            fillMode: Image.PreserveAspectFit
    //            visible: image !== "" && !aniLabel.visible
    //            width: parent.width * (event.type === 'm.sticker' ? 0.3 : 1)
    //            source: image
    //        }

            AnimatedImage {
                id: imgLabel
                source: image
                fillMode: Image.PreserveAspectFit
                visible: image !== "" && status === Image.Ready
                width: parent.width * (event.type === 'm.sticker' ? 0.3 : 1)
                playing: false
                // autoTransform: true
                MouseArea {
                    anchors.fill: parent
                    onClicked: parent.playing = !parent.playing
                }
            }

            Label {
                id: msgLabel
                visible: image === ""

                anchors {
                    left: parent.left
                    right: parent.right
                }
                horizontalAlignment: event.type === "m.room.redaction" ? Text.AlignHCenter : Text.AlignLeft
                text: {
                    if (event.type === "m.room.message") {
                        if (event.content.format) {
                            if (event.content.format === 'org.matrix.custom.html') {
                                return bluepill.htmlcss + event.content.formatted_body
                            }
                        }
                        return event.content.body
                    } else if (event.type === "m.sticker") {
                        return ""
                    } else if (event.type === "m.room.encrypted") {
                        return qsTr("End to end encryption not implemented")
                    } else if (event.type === "m.room.redaction") {
                        return qsTr("redacted") + (event.content.reason !== "" ? ": " + event.content.reason : "")
                    } else if (event.type === "m.room.member") {
                            if (event.content.membership === "leave") {
                                return qsTr("left the room")
                            }

                            if (event.unsigned.prev_content) {
                                return qsTr("is now ") + event.content.displayname
                            }

                            if (event.content.membership === "join") {
                                return qsTr("entered the room")
                            }
                    } else {
                        return "unknown: " + event.type
                    }
                }
                wrapMode: Text.WordWrap
                // height: contentHeight
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall * fontSizeConf.value
                linkColor: Theme.highlightColor
                textFormat: Text.RichText
                onLinkActivated: Qt.openUrlExternally(link)
                Rectangle {
                    anchors.fill: parent
                    radius: height * 0.5
                    color: "grey"
                    z: -1
                    visible: event.type === "m.room.redaction"
                    opacity: 0.2
                }
            }
            Row {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: Theme.iconSizeExtraSmall
            }
        }
    }
}
