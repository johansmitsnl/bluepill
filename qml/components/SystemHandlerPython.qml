import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: systemhandler

    signal hostName(string hostname)
    signal loggedIn(string token, string userId)
    signal isStarted()
    signal loginFailed()
    signal loggedOut()
    signal roomsList(var data)
    signal roomEvents(var events)
    signal userInfo(var userdata)
    signal messageSent()
    signal newRoomEvent(string room_id, var event)
    signal matrixHttpError(string errorstring, var err)
    signal matrixRequestError(var err)

    Component.onCompleted: {
        setHandler("hostName", hostName)
        setHandler("loggedIn", loggedIn)
        setHandler("isStarted", isStarted)
        setHandler("loginFailed", loginFailed)
        setHandler("loggedOut", loggedOut)
        setHandler("roomsList", roomsList)
        setHandler("roomEvents", roomEvents)
        setHandler("userInfo", userInfo)
        setHandler("messageSent", messageSent)
        setHandler("newRoomEvent", newRoomEvent)
        setHandler("matrixHttpError", matrixHttpError)
        setHandler("matrixRequestHandler", matrixRequestError)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('SystemHandler', function () {
            console.log('SystemHandler is now imported')
            initSystem()
            bluepill.engineLoaded = true
        })
    }

    onMatrixHttpError: {
        console.error("Matrix Http Error: " + errorstring )
        console.error(err)
    }

    onMatrixRequestError: {
        console.error("Matrix Request Error")
        console.error(err)
    }

    function initSystem() {
        console.log('system_init')
        call("SystemHandler.system_init", [bluepill], function() {})
    }

    function doStart() {
        console.log('doStart')
        call("SystemHandler.do_start", function() {})
    }

    function getHostname() {
        call("SystemHandler.systemhandler.gethostname", function() {})
        // call("SystemHandler.get_hostname", function() {})
    }

    function doLogin(user, password, hostname, homeserver, identserver) {
        // call("SystemHandler.systemhandler.dologin", [user, password, hostname, homeserver, identserver], function() {})
        call("SystemHandler.do_login", [user, password, hostname, homeserver, identserver], function() {})
    }

    function doLogout() {
        // call("SystemHandler.systemhandler.dologout", function() {})
        call("SystemHandler.do_logout", function() {})
    }

    function getRooms() {
        call("SystemHandler.systemhandler.getrooms", function() {})
        // call("SystemHandler.get_rooms", function() {})
    }

    function startRoomListener(room_id) {
        // call("SystemHandler.systemhandler.startroomlistener", [room_id], function() {})
        call("SystemHandler.start_room_listener", [room_id], function() {})
    }

    function stopRoomListener(room_id) {
        // call("SystemHandler.systemhandler.stoproomlistener", [room_id], function() {})
        call("SystemHandler.stop_room_listener", [room_id], function() {})
    }

    function getRoomEvents(room_id) {
        // call("SystemHandler.systemhandler.getroomevents", [room_id], function() {})
        call("SystemHandler.get_room_events", [room_id], function() {})
    }

    function getUserInfo() {
        // call("SystemHandler.systemhandler.getuserinfo", function() {})
        call("SystemHandler.get_user_info", function() {})
    }

    function sendMessage(room_id, message) {
        call("SystemHandler.systemhandler.sendmessage", [room_id, message], function() {})
        // call("SystemHandler.send_message", [room_id, message], function() {})
    }

    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
}
