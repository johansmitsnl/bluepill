#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Client handler for Bluepill
"""

import pyotherside
import threading
import sys
import os

sys.path.append("/usr/share/harbour-bluepill/python")

from bluepill.client import BluepillClientFactory
from bluepill.memory import Memory


def get_hostname():
    """
    Return System hostname
    """

    hostname = os.uname().nodename
    pyotherside.send("hostName", hostname)


def do_init():
    """
    Instanciate client and connect to the homeserver
    """

    BluepillClientFactory().get_client().connect()


def do_login(user, password, hostname, homeserver, identserver):
    """
    Login
    """

    client = BluepillClientFactory().get_client()
    if not client:
        pyotherside.send("No BluepillClient initiated")
    client.login(user, password, hostname, homeserver, identserver)


def get_rooms():
    """
    Get list of my rooms
    """

    client = BluepillClientFactory().get_client()

    data = []

    for roominfo in client.get_roomlist():
        data.append(roominfo)

    data.sort(key=lambda r: r["time"], reverse=True)
    pyotherside.send("roomsList", data)


def get_room(room_id):
    """
    Get single room info
    """

    client = BluepillClientFactory().get_client()
    if client:
        rd = client.get_room(room_id)
        if rd:
            pyotherside.send("roomData", rd)


def get_room_events(room_id):
    """
    Get room events from room object
    """

    BluepillClientFactory().get_client().get_room_events(room_id)


def get_room_members(room_id):
    """
        get room events for room_id
        """

    rd = Memory().get_object(room_id)
    members = rd.members
    pyotherside.send("roomMembers", room_id, members)


def join_room(room_id_or_alias: str):
    """
    Join a room
    """

    if BluepillClientFactory().get_client().join_room(room_id_or_alias):
        get_rooms()


def get_user_info():
    """
    Get user data of logged in user
    """

    BluepillClientFactory().get_client().get_user_info()


def send_message(room_id, message):
    """
    Send a message
    """

    BluepillClientFactory().get_client().send_message(room_id, message)
    pyotherside.send("messageSent")


def send_image(room_id, image_file):
    """
    Send an image
    """

    BluepillClientFactory().get_client().send_image(room_id, image_file)
    pyotherside.send("imageSent")


def redact_message(room_id, event_id, reason=None):
    """
    redact a message
    """

    BluepillClientFactory().get_client().redact(room_id, event_id, reason)


def seen_message(room_id):
    """
    Mark seen messages
    """

    BluepillClientFactory().get_client().seen_message(room_id)


def is_typing(room_id, timeout=30000):
    """
    User is typing
    """

    BluepillClientFactory().get_client().is_typing(room_id, timeout)


def restart_listener():
    """
    Start the listener thread (after crash?)
    """

    BluepillClientFactory().get_client().restart_listener()


def do_logout():
    """
    Logout
    """

    BluepillClientFactory().get_client().logout()


def get_unread_count(room_id):
    BluepillClientFactory().get_client().get_unread_count(room_id)


class ClientHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

        self.bgthread1 = threading.Thread()
        self.bgthread1.start()

        self.bgthread2 = threading.Thread()
        self.bgthread2.start()

        pyotherside.atexit(self.doexit)

    def getrooms(self):
        if self.bgthread1.is_alive():
            return
        self.bgthread1 = threading.Thread(target=get_rooms)
        self.bgthread1.start()

    def getroom(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_room, args=[room_id])
        self.bgthread.start()

    def getroomevents(self, room_id):
        if self.bgthread2.is_alive():
            return
        self.bgthread2 = threading.Thread(
            target=get_room_events, args=[room_id]
        )
        self.bgthread2.start()

    def joinroom(self, room_id_or_alias):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=join_room, args=[room_id_or_alias]
        )
        self.bgthread.start()

    def getroommembers(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=get_room_members, args=[room_id]
        )
        self.bgthread.start()

    def sendmessage(self, room_id, message):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=send_message, args=[room_id, message]
        )
        self.bgthread.start()

    def sendimage(self, room_id, image_file):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=send_image, args=[room_id, image_file]
        )
        self.bgthread.start()

    def redactmessage(self, room_id, event_id, reason=None):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=redact_message, args=[room_id, event_id, reason]
        )
        self.bgthread.start()

    def seenmessage(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=seen_message, args=[room_id])
        self.bgthread.start()

    def istyping(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=is_typing, args=[room_id])
        self.bgthread.start()

    def getunreadcount(self, room_id):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=get_unread_count, args=[room_id]
        )
        self.bgthread.start()

    def getuserinfo(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_user_info)
        self.bgthread.start()

    def dologout(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=do_logout)
        self.bgthread.start()

    def doexit(self):
        """
        Exit the system on leaving app
        """

        BluepillClientFactory().get_client().do_exit()


clienthandler = ClientHandler()
