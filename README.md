# Bluepill - stay in the matrix

For the brave: How can I compile it for my own with Sailfish SDK
(actual 2.0)

 * Start up QtCreator (Sailfish SDK)
 * Open the project by selecting the `harbour-bluepill.pro` file
 * Select both arm7 and i486 kits (i486 is for the emulator, arm for the device)
 * (for a device) connect it via USB, select "Developer mode"
 * on QtCreator you need to open Extras -> Settings
 * Tab "Devices", Add...
 * Sailfish Device
 * Physical device
 * Enter the address the device shows you in it's developer mode settings and enter the password
 * QtCreator on the left hand side select your device or the emulator
 * Select the arm kit for your device the i486 kit for the emulator

To run it on your device/emulator

 * Select "Deploy as RPM package"
 * Start the compilation with Ctrl-R

To build an RPM for your device

 * Select "Build RPM Package for manual deployment"
 * Click "Build->Deploy project 'harbour-bluepill'"
 * it then pops up where you find the rpm
