"""
Bluepill client functionality
"""

import pyotherside
import sys
import os
from urllib.parse import urlparse
from urllib.parse import quote
import re

sys.path.append("../")

from matrix_client.client import MatrixClient
from matrix_client.errors import MatrixRequestError, MatrixUnexpectedResponse

from bluepill.singleton import Singleton
from bluepill.client_data import ClientDataFactory
from bluepill.client_data import CLIENTDATA
from bluepill.room_data import RoomDataFactory
from bluepill import util
from bluepill.memory import Memory
from markdown2 import Markdown
import emoji


STORE_NAME = "BluepillClient"


class BluepillClient:
    """
    Bluepill Client
    """

    def __init__(self):
        """
        Initialization of the client
        """

        # Connectivity data

        self._data = ClientDataFactory().clientData

        # Matrix connector data

        self._client = None  # the Matrix Client
        self._matrix = None  # Matrix API
        # self._room_listener_uid = None    # Room listener
        self._m_room_message_listener = None
        self._m_room_sticker_listener = None
        self._m_redaction_listener = None
        self._m_typing_listener = None
        self._m_receipt_listener = None

        link_patterns = [
            (
                re.compile(
                    r"((([@!#]?[A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+(:[0-9]+)?|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)"
                ),
                r"\1",
            )
        ]
        self._markdown = Markdown(
            extras=["link-patterns"], link_patterns=link_patterns
        )

    def logout(self):
        """
        Log system out
        """

        self._client.logout()
        self._client = None
        ClientDataFactory().client_data = None
        Memory().delete_object(CLIENTDATA)
        self._data = ClientDataFactory().clientData
        pyotherside.send("loggedOut")

    def login(
        self,
        user: str,
        password: str,
        hostname: str,
        homeserver: str,
        identserver: str,
    ):
        """
        Login to the server. Will initialize token
        """

        pyotherside.send("login")
        try:
            self._client = MatrixClient(homeserver)
            self._data.token = self._client.login(
                user, password, limit=100, sync=True
            )
            pyotherside.send("gotToken", self._data.token)
        except MatrixRequestError:
            pyotherside.send("loginFailed")
            return

        pyotherside.send("loggedIn")

        self._data.homeserver = homeserver
        self._data.identserver = identserver
        self._data.user_id = self._client.user_id
        self._data.device_id = self._client.device_id
        self._data.encryption = True
        user = self._client.get_user(self._data.user_id)
        self._data.user_name = user.get_display_name()
        avatar_url = user.get_avatar_url()
        self._data.user_avatar_url = (
            self._get_url(avatar_url) if avatar_url else ""
        )

        response = self._client.api.sync()

        self.get_room_dir(response)
        self.get_rooms()
        self._data.save()

        self.start_room_event_listener()
        self.start_listener()

        pyotherside.send("initReady", self._data.user_id)
        pyotherside.send("log", "token", self._data.token)

    def connect(self):
        """
        Connect to the matrix server. If no token exists, signal needLogin
        """

        if not self._data.token:
            pyotherside.send("needLogin")
            return

        pyotherside.send("log", "token", self._data.token)
        try:
            self._client = MatrixClient(
                self._data.homeserver,
                user_id=self._data.user_id,
                token=self._data.token,
            )
        except:
            pyotherside.send("connectFailed")
            self._client = None
            pyotherside.send("initReady", self._data.user_id)
            return

        pyotherside.send("isConnected")

        self.sync()
        self.sync_rooms()
        self.start_room_event_listener()
        self.start_listener()
        pyotherside.send("initReady", self._data.user_id)

    def do_exit(self):
        """
        Stop the listener Threads
        """

        self.stop_listener()
        self.stop_room_event_listener()
        self.save()

    def join_room(self, room_id_or_alias: str) -> bool:
        """
        Join the room
        """

        try:
            room = self._client.join_room(room_id_or_alias)
        except MatrixRequestError as e:
            pyotherside.send("joinRoomFailed", e.content)
            return False
        if not room:
            return False

        self._create_room(room_id=None, rd=room)
        self._data.roomdir.append(room.room_id)
        self._data.save()
        return True

    def get_room_dir(self, response):
        """
        Get direct rooms from account_data
        """

        account_data = response["account_data"]
        event = next(
            filter(lambda x: x["type"] == "m.direct", account_data["events"]),
            None,
        )
        if event:
            pyotherside.send("log", "m.direct in account_data")
            self._data.roomdir = [
                j for i in event["content"].values() for j in i
            ]

    def format_event(self, room_id: str, event):
        """
        TODO: event.py?
        """

        image = ""
        user_id = event["sender"]
        user = self._client.get_user(user_id)
        try:
            avatar_url = user.get_avatar_url()
        except:
            avatar_url = ""
        avatar_url = self._get_url(avatar_url) if avatar_url else ""
        dat = event["origin_server_ts"] / 1000
        event["origin_server_ts"] = dat
        try:
            name = user.get_friendly_name()
        except:
            name = ""
        try:
            if event["content"]["msgtype"] == "m.image":
                image = event["content"]["url"]
                pyotherside.send("image: ", image)
                image = (
                    self._get_url(self._client.api.get_download_url(image))
                    if image
                    else ""
                )
        except:
            image = ""

        try:
            if event["type"] == "m.sticker":
                image = event["content"]["url"]
                if image:
                    image = self._get_url(
                        self._client.api.get_download_url(image)
                    )
        except:
            image = ""

        try:
            if event["content"]["msgtype"] == "m.text":
                event["content"]["body"] = emoji.emojize(
                    event["content"]["body"], use_aliases=True
                )
                event["content"]["formatted_body"] = self._markdown.convert(
                    event["content"]["body"]
                )
                event["content"]["format"] = "org.matrix.custom.html"
        except:
            pass
        room_data = RoomDataFactory().room_data(room_id)
        if room_data:
            room_name = room_data.display_name
        else:
            room_name = ""

        asect = util.format_full_date(dat)
        return (
            dat,
            {
                "event": event,
                "name": name,
                "rname": room_name,
                "user_id": user_id,
                "edate": dat,
                "avatar_url": avatar_url,
                "image": image,
                "asect": asect,
            },
        )

    def room_append_event(self, room_id: str, event, save: bool = True):
        """
        Append a room event
        TODO: to room.py
        """

        room_data = RoomDataFactory().room_data(room_id)
        if not room_data:
            return None
        room_name = room_data.display_name
        room_level = room_data.level

        image = ""
        user_id = event["sender"]
        user = self._client.get_user(user_id)
        try:
            avatar_url = user.get_avatar_url()
        except:
            avatar_url = ""
        avatar_url = self._get_url(avatar_url) if avatar_url else ""
        dat = event["origin_server_ts"] / 1000
        event["origin_server_ts"] = dat
        try:
            name = user.get_friendly_name()
        except:
            name = ""
        try:
            if event["content"]["msgtype"] == "m.image":
                image = event["content"]["url"]
                pyotherside.send("image: ", image)
                image = (
                    self._get_url(self._client.api.get_download_url(image))
                    if image
                    else ""
                )
        except:
            image = ""

        try:
            if event["type"] == "m.sticker":
                image = event["content"]["url"]
                if image:
                    image = self._get_url(
                        self._client.api.get_download_url(image)
                    )
        except:
            image = ""

        try:
            if event["content"]["msgtype"] == "m.text":
                event["content"]["body"] = emoji.emojize(
                    event["content"]["body"], use_aliases=True
                )
                event["content"]["formatted_body"] = self._markdown.convert(
                    event["content"]["body"]
                )
                event["content"]["format"] = "org.matrix.custom.html"
        except:
            pass

        asect = util.format_full_date(dat)

        event_data = {
            "event": event,
            "name": name,
            "rname": room_name,
            "rlevel": room_level,
            "user_id": user_id,
            "edate": dat,
            "avatar_url": avatar_url,
            "image": image,
            "asect": asect,
        }
        room_data.events.append(event_data)
        room_data.last_time = dat
        if save:
            room_data.end_token = self._client.api.get_room_messages(
                room_id, None, "f"
            )["end"]
            room_data.save()
        return event_data

    def _create_room(self, room_id: str = None, rd=None):
        """
        Create a room_id
        """

        if rd and not room_id:
            room_id = rd.room_id
        else:
            rd = self._client.rooms[room_id]

        room_data = RoomDataFactory().room_data(room_id)
        room_data.room_id = room_id
        room_data.name = rd.name
        pyotherside.send("syncingRooms", rd.display_name)
        room_data.display_name = rd.display_name
        room_data.topic = rd.topic
        tags = rd.get_tags()["tags"]
        for tag in tags:
            if tag == "m.favourite":
                room_data.level = "fav"
                room_data.lorder = tags[tag].get("order", 0)
            elif tag == "m.lowpriority":
                room_data.level = "low"
            else:
                room_data.level = "normal"

        events = rd.get_events()
        room_data.events = []
        for event in events:
            self.room_append_event(room_id, event, save=False)

        user_id = ""
        if room_id in self._data.roomdir:
            for member in rd.get_joined_members():
                if member.user_id != self._data.user_id:
                    try:
                        avatar_url = member.get_avatar_url()
                        user_id = member.user_id
                        if avatar_url:
                            room_data.avatar_url = self._get_url(avatar_url)
                    except:
                        avatar_url = ""
                        user_id = ""
        else:
            for e in self._client.api.get_room_state(room_id):
                if e["type"] == "m.room.avatar":
                    aurl = e["content"]["url"]
                    if aurl:
                        avatar_url = self._client.api.get_download_url(aurl)
                        room_data.avatar_url = self._get_url(avatar_url)
            user_id = room_id

        room_data.user_id = user_id
        room_data.encrypted = rd.encrypted
        room_data.end_token = self._client.api.get_room_messages(
            room_id, None, "f"
        )["end"]
        room_data.start_token = self._client.api.get_room_messages(
            room_id, None, "f"
        )["start"]
        room_data.members = self.get_room_members(room_id)

        room_data.save()

    def _sync_room(self, room_id: str):
        """
        Sync room data
        """

        rd = self._client.rooms[room_id]
        room_data = RoomDataFactory().room_data(room_id)
        if not room_data.room_id:
            self._create_room(room_id)
            return

        room_data.name = rd.name
        room_data.display_name = rd.display_name
        room_data.topic = rd.topic
        tags = rd.get_tags()["tags"]
        for tag in tags:
            if tag == "m.favourite":
                room_data.level = "fav"
                room_data.lorder = tags[tag]["order"]
            elif tag == "m.lowpriority":
                room_data.level = "low"
            else:
                room_data.level = "normal"
        events = self._client.api.get_room_messages(
            room_id, room_data.end_token, "f"
        )["chunk"]
        for event in events:
            pyotherside.send("newEvent")
            self.room_append_event(room_id, event, save=False)

        room_data.end_token = self._client.api.get_room_messages(
            room_id, None, "f"
        )["end"]
        room_data.save()

    def sync_rooms(self):
        """
        Synchronize rooms from last time
        """

        self._data.rooms = []
        for room in self._client.rooms:
            self._sync_room(room)
            self._data.rooms.append(room)

        pyotherside.send("initReady", self._data.user_id)

    def get_rooms(self):
        """
        Initial room fetching and preparing room_data objects
        """

        for room in self._client.rooms:
            self._create_room(room)
            self._data.rooms.append(room)

    def get_roomlist(self):
        """
        Get roomlisst
        """

        for room in self._data.rooms:
            rd = Memory().get_object(room)
            pyotherside.send("log", "outputting", rd.room_id)
            if rd:
                levent = rd.events[len(rd.events) - 1] if rd.events else None
                yield {
                    "room_id": rd.room_id,
                    "user_id": rd.user_id,
                    "room_name": rd.display_name,
                    "prio": rd.level if rd.level else "",
                    "direct": room in self._data.roomdir,
                    "avatar_url": rd.avatar_url,
                    "time": rd.last_time,
                    "encrypted": rd.encrypted,
                    "last_event": levent,
                }

    def get_room(self, room_id: str):
        """
        get single room data
        """
        rd = Memory().get_object(room_id)
        if rd:
            return {
                "room_id": rd.room_id,
                "user_id": rd.user_id,
                "room_name": rd.display_name,
                "prio": rd.level,
                "direct": room_id in self._data.roomdir,
                "avatar_url": rd.avatar_url,
                "time": rd.last_time,
                "encrypted": rd.encrypted,
            }

        return None

    def listener_exception(self, e):
        """
        Listener exception
        """

        pyotherside.send("Listener exception")
        # pyotherside.send("restartListener")

    def seen_message(self, room_id: str):
        """
        Send message receipt
        """

        rd = RoomDataFactory().room_data(room_id)

        if not rd:
            return

        levent = rd.events[len(rd.events) - 1]["event"]

        try:
            lr = rd.last_read
        except:
            lr = 0
        if lr < levent["origin_server_ts"]:
            pyotherside.send("seen_message")
            rd.last_read = levent["origin_server_ts"]
            self._client.api.send_read_markers(
                room_id, levent["event_id"], levent["event_id"]
            )
            rd.save()

    def is_typing(self, room_id: str, timeout: int = 30000):
        """
        Send typing
        Args:
            room_id(str): The room ID.
            timeout: Timeout of typing
        """

        content = {"typing": True, "timeout": timeout}
        path = "/rooms/{}/typing/{}".format(
            quote(room_id), quote(self._data.user_id)
        )
        self._client.api._send("PUT", path, content)

    def start_listener(self):
        """
        start listener_thread
        """

        try:
            self._client.start_listener_thread(
                timeout_ms=60000, exception_handler=self.listener_exception
            )
        except:
            pyotherside.send("Starting listener thread failed")
            pyotherside.send("restartListener")

    def restart_listener(self):
        """
        Restart the listener thread
        """

        try:
            self.stop_listener()
        except:
            pyotherside.send("log", "restartListener: cannot stop listener")
        try:
            self.start_listener()
        except:
            pyotherside.send("log", "restartListener: cannot start listener")

    def stop_listener(self):
        """
        stop the listener_thread
        """

        try:
            self.client._stop_listener_thread()
        except:
            pyotherside.send("Stopping listener thread failed")

    def room_listener(self, event):
        """
        The room listener
        """

        room_id = event["room_id"]
        rd = Memory().get_object(room_id)
        if event["type"] == "m.room.redaction":
            count = 0
            for ev in rd.events:
                if ev["event"]["event_id"] == event["redacts"]:
                    # event["event_id"] = event["redacts"]
                    dat, rd.events[count] = self.format_event(room_id, event)
                    rd.last_event = dat
                    rd.save()
                    pyotherside.send("mRedactEvent", room_id, event)
                    return
                count += 1

        event_data = self.room_append_event(room_id, event)
        if event_data["user_id"] == self._data.user_id:
            rd.set_revent(event_data["event"]["event_id"])

        pyotherside.send("mRoomEvent", room_id, event_data)
        pyotherside.send("roomUnread", event["room_id"], rd.unread_count)

    def receipt_listener(self, event):
        """
        A user has read some event
        """

        for receipt in event["content"]:
            for uid in event["content"][receipt]["m.read"]:
                if uid == self._data.user_id:
                    rd = Memory().get_object(event["room_id"])
                    rd.set_revent(receipt)
                    pyotherside.send(
                        "roomUnread", event["room_id"], rd.unread_count
                    )

    def get_unread_count(self, room_id: str):
        rd = Memory().get_object(room_id)

        if rd:
            pyotherside.send("roomUnread", room_id, rd.unread_count)

    def typing_listener(self, event):
        """
        A user is typing (or not)
        """

        if len(event["content"]["user_ids"]) == 0:
            pyotherside.send("stopTyping", event["room_id"])
            return

        users = []

        for user_id in event["content"]["user_ids"]:
            user = self._client.get_user(user_id)
            user_name = user.get_display_name()
            avatar_url = user.get_avatar_url()
            if not avatar_url:
                image = ""
            else:
                image = self._get_url(avatar_url)

            users.append(
                {"user_id": user_id, "user_name": user_name, "image": image}
            )
        pyotherside.send("startTyping", event["room_id"], users)

    def stop_room_event_listener(self):
        """
        Leave listener
        """

        self._client.remove_listener(self._m_room_message_listener)
        self._client.remove_listener(self._m_room_sticker_listener)
        self._client.remove_listener(self._m_redaction_listener)
        self._client.remove_ephemeral_listener(self._m_typing_listener)
        self._client.remove_ephemeral_listener(self._m.receipt_listener)
        self._m_room_message_listener = None
        self._m_room_sticker_listener = None
        self._m.redaction_listener = None
        self._m_typing_listener = None
        self._m_receipt_listener = None

    def start_room_event_listener(self):
        """
        set room listener
        """

        if self._m_room_message_listener:
            self._client.remove_listener(self._m_room_message_listener)
        if self._m_room_sticker_listener:
            self._client.remove_listener(self._m_room_sticker_listener)
        if self._m_redaction_listener:
            self._client.remove_listener(self._m_redaction_listener)
        self._m_room_message_listener = self._client.add_listener(
            self.room_listener, event_type="m.room.message"
        )
        self._m_room_sticker_listener = self._client.add_listener(
            self.room_listener, event_type="m.sticker"
        )
        self._m_redaction_listener = self._client.add_listener(
            self.room_listener, event_type="m.room.redaction"
        )
        self._m_typing_listener = self._client.add_ephemeral_listener(
            self.typing_listener, event_type="m.typing"
        )
        self._m_receipt_listener = self._client.add_ephemeral_listener(
            self.receipt_listener, event_type="m.receipt"
        )

    def get_room_members(self, room_id: str):
        """
        get members and event list for room_id
        """

        members = []
        for member in self._client.api.get_room_members(room_id)["chunk"]:
            try:
                if member["content"]["avatar_url"]:
                    image = self._get_url(member["content"]["avatar_url"])
                else:
                    image = ""
            except:
                image = ""

            try:
                member["origin_server_ts"] /= 1000
                members.append(
                    {
                        "event_id": member["event_id"],
                        "name": member["content"]["displayname"],
                        "avatar_url": image,
                        "time": member["origin_server_ts"],
                    }
                )
            except:
                pass

        return members

    def get_room_events(self, room_id: str):
        """
        get room events for room_id
        """

        rd = Memory().get_object(room_id)
        events = rd.get_room_events()
        members = rd.members
        pyotherside.send("roomEvents", events, members)

    def room_member_list(self, room_id: str):
        """
        Send room member list
        """
        rd = Memory().get_object(room_id)
        pyotherside.send("roomMembers", rd.members)

    def get_user_info(self):
        pyotherside.send(
            "userInfo",
            {
                "lname": self._data.user_name,
                "user_id": self._data.user_id,
                "avatar_url": self._data.user_avatar_url,
            },
        )

    def _get_url(self, url: str):
        """
        """

        if url == "":
            return url

        f = urlparse(url).path
        filename = f[f.rfind("/") + 1 :]
        path = os.path.join(Memory().cache_home, filename)
        if not os.path.exists(path):
            pyotherside.send("get_url: need to download", path)
            try:
                util.dl_from_url(url, path)
            except:
                return ""

        return path

    def sync(self, first: bool = False):
        """
        Sync with server
        """

        pyotherside.send("Client data syncer")
        try:
            syncdata = self._client.api.sync(
                since=self._ts_token, full_state=first
            )
        except:
            syncdata = None

        if not syncdata:
            pyotherside.send("Matrix syncing failed")
            return

        event = next(
            filter(
                lambda x: x["type"] == "com.gitlab.cy8aer.bluepill",
                None
                # account_data["events"],
            ),
            None,
        )
        if event:
            pyotherside.send(
                "log", "com.gitlab.cy8aer.bluepill in account_data"
            )
            pass

        for room in syncdata["rooms"]["join"]:
            # configuration of rooms
            pass

        for room in syncdata["rooms"]["invite"]:
            # you are invited to new rooms
            pass

        for room in syncdata["rooms"]["leave"]:
            # you left rooms or you have been banned from a room
            pass

        for presence in syncdata["presence"]["events"]:
            # presence of users
            # content:
            #     currently_active
            #     last_active_ago
            #     presence
            # sender
            # type
            pass

        # device_lists

        self.get_room_dir(syncdata)

        # device_time_keys_count

    def save(self):
        """
        Save myself
        """

        # Memory().save_object(STORE_NAME, self)

    # Data providers

    def send_message(self, room_id: str, message: str):
        """
        Markdown transform a message and send it
        """

        message = emoji.emojize(message, use_aliases=True)
        pyotherside.send("send_message", room_id, message)
        htmlmsg = self._markdown.convert(message)
        room = self._client.rooms[room_id]
        pyotherside.send("sending message ", htmlmsg, message)
        room.send_html(htmlmsg, body=message)

    def send_image(self, room_id: str, image_file: str):
        """
        upload an image and send a message
        """

        mimes = {
            "jpg": "image/jpeg",
            "jpeg": "image/jpeg",
            "gif": "image/gif",
            "png": "image/png",
        }

        ext = os.path.splitext(image_file)[1][1:]
        content_type = mimes[ext]

        pyotherside.send("log", "uploading image")
        try:
            with open(image_file, "rb") as image:
                mxc = self._client.upload(image.read(), content_type)
        except MatrixUnexpectedResponse:
            pyotherside.send(
                "imageSendFailed", "image upload unexpected response"
            )
            return
        except MatrixRequestError:
            pyotherside.send("imageSendFailed", "image upload request error")
            return
        except:
            pyotherside.send("imageSendFailed", "other error")
            return

        pyotherside.send("log", "send image message")
        room = self._client.rooms[room_id]
        try:
            room.send_image(mxc, os.path.split(image_file)[1])
        except:
            pyotherside.send("imageSendFailed", "image message failed")

        pyotherside.send("imageSent")

    def redact(self, room_id: str, event_id: str, reason: str = None):
        """
        Redact a message
        """

        rd = self._client.rooms[room_id]
        rd.redact_message(event_id, reason)

    @property
    def user_name(self):
        """
        Return the user_name
        """

        return self._user_name

    @property
    def user_id(self):
        """
        Return the user_id
        """

        return self._user_id


class BluepillClientFactory(metaclass=Singleton):
    """
    Factory which creates a BluepillClient if it does not exist
    """

    def __init__(self):
        """
        Initialization
        """

        self._bluepill_client = None

    def get_client(self):
        """
        get the Bluepill Client
        """

        if not self._bluepill_client:
            self._bluepill_client = BluepillClient()

        return self._bluepill_client
